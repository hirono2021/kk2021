#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
import re
import datetime
import requests
from bs4 import BeautifulSoup

response = requests.get('http://hirono-hideki.info/wp/?p=1699')
soup = BeautifulSoup(response.text, 'html.parser')
keys = [key.strip().split(' -> ')[0] for key in soup.find('div', {'class': 'entry-body'}) if re.search('.+ -> .+', str(key))]

now = datetime.datetime.now()
create_time = now.strftime('%Y-%m-%d %H:%M:%S')

src_file = "./list.txt"
subprocess.call("nkf -Lu --overwrite {f}".format(f=src_file), shell=True)

with open(src_file) as f:
    for line in f:
        line = line.rstrip()  # 読み込んだ行の末尾には改行文字があるので削除
        arg = re.match(r"^mkdir -p (.+)$", line)
        path = ""
        str = ""
        if arg:
            path = arg.group(1)
            str = """
if [ ! -d ./{d} ]; then
    mkdir -p {d}
    echo "new_create_Directory: {path}"
fi
            """.format(d=path, path=path)
        print(str)

with open(src_file) as f:
    for line in f:
        line = line.rstrip()  # 読み込んだ行の末尾には改行文字があるので削除
        arg = re.match(r"^touch (.+)$", line)
        path = ""
        ch = """
- [x] 編集中
- [ ] ブログ投稿済み
- [ ] 印刷済
        """

        str = ""
        if arg:
            path = arg.group(1)

            title = path
            tags = ""
            for k in keys:
                matchs = ""
                if k in title:
                    if not re.match(k, matchs):
                        matchs = matchs + k
                        tags = tags + f" #{k}"

            str = """
if [ ! -f ./{path} ]; then
    touch {path}
    echo -e "#CREATE_TIME:{d}\\n\\n" >> {path}
    echo "# {f}" >> {path}
    echo  -e "\\n:CATEGORIES: @kanazawabengosi #金沢弁護士会 @JFBAsns 日本弁護士連合会（日弁連） #法務省 @MOJ_HOUMU{m}\\n{ch}\\n\\n\\n\\n\\n\\n" >> {path}
    echo "new_create_File: {path}"
fi

             """.format(f=path.replace('/','／').replace('.md', ''), path=path, d=create_time, ch=ch.rstrip(), m=tags)

        print(str)


